let express = require('express');
let cors = require('cors')
let ping = require('ping');

const { exec } = require("child_process");
let app = express();

app.use(cors(), express.json())
const port = 3000

app.post('/ping', async (req, res) => {
    let result = [];
    let count = 1;
    if(req.body.count > count) { count = req.body.count}
    for(let i = 1; i<= count; i++) {
        let ms = await doPing(req.body.target);
        // console.log('result ' + result)/////
        result.push(ms)
    }
    // console.log('presult', result)///////
    res.json(result)
});

app.post('/speedtest', async (req, res) => {
    console.log('executing test...')/////
    exec("fast --single-line -u", (error, stdout, stderr) => {

        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
        result = stdout;
        res.json(result)
        // console.log(result + 'end')
    });
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});

const doPing = async (target) => {
    let res = await ping.promise.probe(target);
    // console.log('32',res.time)/////
    return res.time
}
